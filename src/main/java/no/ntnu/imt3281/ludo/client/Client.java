package no.ntnu.imt3281.ludo.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Client extends Application {
	
	/**
     * Java.util Logger for handling exceptions and such
     */
    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private static ExecutorService executorService;
    private String name;
	private Socket connection;
	private String token;
	private BufferedReader input;
	private BufferedWriter output;
	public static ClientConnection cc;
    
    /**
     * Will load the login/register screen and set the scene for the whole application.
     */
    @Override
	public void start(Stage primaryStage) {
			try {
				ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
				AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("../gui/LoginScreen.fxml"), bundle);
				Scene scene = new Scene(root);
				primaryStage.setScene(scene);
				primaryStage.show();
			} catch(Exception e) {
				LOGGER.log(Level.SEVERE, e.toString(), e);
			}
	}

	public static void startCheckIncomingMessages() {
		executorService = Executors.newCachedThreadPool();
		LOGGER.log(Level.INFO, "Started thread in client.");
		executorService.execute(() -> {
			while (true) {
				try {
					
					BufferedReader input = new BufferedReader(new InputStreamReader(cc.getConnection().getInputStream()));
					String message = null;
					 if (input.ready()) {
						 message = input.readLine();
				     }

					if (message != null) {
						LOGGER.log(Level.INFO, "Found message in clients input.");
						if (message.startsWith("GLOBAL>")) {
							LOGGER.log(Level.INFO, "Got a global message.");
						}
					}  
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, e.toString(), e);
				}
			}
		});
	}

	/**
     * Boots the actual Client application. Will first prompt a login/register screen before redirecting to the real application
     * @param args Handled by template
     */
	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * Sets the name of this Client
	 * @param name The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sets the connection for this Client
	 * @param connection The connection to set
	 */
	public void setConnection(Socket connection) {
		this.connection = connection;
	}
	
	/**
	 * Sets the token for this Client
	 * @param token The token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	
	/**
	 * Sets the input stream for this Client
	 * @param input The input stream to set
	 */
	public void setInput(BufferedReader input) {
		this.input = input;
	}
	
	/**
	 * Sets the output stream for this Client
	 * @param output The output stream
	 */
	public void setOutput(BufferedWriter output) {
		this.output = output;
	}
	
	/**
	 * Returns the Clients name
	 * @return The name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the Clients connection
	 * @return The socket connection
	 */
	public Socket getConnection() {
		return connection;
	}
	
	/**
	 * Returns a clients ClientConnection object
	 * @return The ClientConnection object
	 */
	public ClientConnection getClientConnection() {
		return cc;
	}
	
	/**
	 * Returns the Clients token
	 * @return The token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * Returns the Clients input stream
	 * @return The BufferedReader input stream
	 */
	public BufferedReader getInput() {
		return input;
	}
	
	/**
	 * Returns the Clients output stream
	 * @return The BufferedWriter output stream
	 */
	public BufferedWriter getOutput() {
		return output;
	}

	/**
     * Send the given message to the client. Ensures that all messages
     * have a trailing newline and are flushed.
     * 
     * @param text the message to send
     * @throws IOException if an error occurs when sending the message 
     */
    public void sendText(String text) throws IOException {
        output.write(text);
        output.newLine();
        output.flush();
    }
    
    /**
     * Sets a clients connection and starts the thread to listen to any incoming messages here
     * @param cc The ClientConnection object belonging to this client
     */
    public static void SetCC(ClientConnection cc) {
    	Client.cc = cc;
    	startCheckIncomingMessages();
    }
}
