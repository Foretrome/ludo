package no.ntnu.imt3281.ludo.client;

import java.net.Socket;

/**
 * Stores a Client's connection to the server. Needed for any client-server communication
 * @author Tommy
 *
 */
public class ClientConnection {
	private String token;
	private Socket connection;

	/**
	 * Creates the ClientConnection and sets it's token and socket
	 * @param con The socket to the server
	 * @param token The clients token
	 */
	public ClientConnection(Socket con, String token) {
		this.connection = con;
		this.token = token;
	}

	/**
	 * Returns the token for this connections user
	 * @return The token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Returns a socket for this connection
	 * @return The socket connection
	 */
	public Socket getConnection() {
		return connection;
	}
}
