package no.ntnu.imt3281.ludo.logic;

/**
 * This exception is thrown when a player attempts to join a game already containing 4 players.
 * @author Tommy
 */
@SuppressWarnings("serial")
public class NoRoomForMorePlayersException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with the specified detail message. 
	 * @param message The detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method
	 */
	public NoRoomForMorePlayersException(String message) {
		super(message);
	}
}
