package no.ntnu.imt3281.ludo.logic;

/**
 * This exception is thrown when a game is attempted created with fewer than 2 players.
 * @author Tommy
 */
@SuppressWarnings("serial")
public class NotEnoughPlayersException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with the specified detail message. 
	 * @param message the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method
	 */
	public NotEnoughPlayersException(String message) {
		super(message);
	}
}
