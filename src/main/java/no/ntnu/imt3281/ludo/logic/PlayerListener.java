package no.ntnu.imt3281.ludo.logic;

/**
 * Any class that implements the PlayerListener interface must provide an implementation for a 
 * method named playerStateChanged that accepts a PlayerEvent.
 * @author Tommy
 *
 */
public interface PlayerListener {

	void playerStateChanged(PlayerEvent ple);

}
