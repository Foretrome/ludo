package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * Class to handle events caused by a player. This class should always be called when a player state is changed,
 * so an event is made. This enables the listeners of the PlayerEvents interface to receive these events
 * @author Tommy
 */
@SuppressWarnings("serial")
public class PlayerEvent extends EventObject implements PlayerListener {
	
	/**
	 * Integer of value zero represents the PLAYING state for a given player
	 */
	public static final int PLAYING = 0;
	
	/**
	 * Integer of value one represents the WAITING state for a given player
	 */
	public static final int WAITING = 1;
	
	/**
	 * Integer of value two represents the LEFTGAME state for a given player
	 */
	public static final int LEFTGAME = 2;
	
	/**
	 * Integer of value three represents the WON state for a given player
	 */
	public static final int WON = 3;
	int activePlayer;
	int state;
	
	/**
	 * Constructor for PieceEvent. Creates the EventObject
	 * @param ludoObject The object on which the Event initially occurred.
	 */
	public PlayerEvent(Object ludoObject) {
		super(ludoObject);
	}

	/**
	 * Constructor for PlayerEvent. Creates the EventObject and sets activePlayer and state
	 * @param ludoObject The object on which the Event initially occurred.
	 * @param player The player who created the Event
	 * @param state The state that has changed
	 */
	public PlayerEvent(Ludo ludoObject, int player, int state) {
		super(ludoObject);
		setActivePlayer(player);
		setState(state);
	}

	/**
	 * Sets the activePlayer of this object
	 * @param activePlayer The player to be set as active
	 */
	public void setActivePlayer(int activePlayer) {
		this.activePlayer = activePlayer;
	}
	
	/**
	 * Sets the state of this object
	 * @param state The state to be set
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * Returns the activePlayer value of this object
	 * @return The activePlayer value
	 */
	public int getActivePlayer() {
		return activePlayer;
	}
	
	/**
	 * Returns the state value of this object
	 * @return The state value
	 */
	public int getState() {
		return state;
	}

	@Override
	public void playerStateChanged(PlayerEvent ple) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Compares two PlayerEvent objects. Needed for the test to check order of events
	 * @param obj The object to compare to 'this'
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PlayerEvent)) {
			return false;
		}
		PlayerEvent that = (PlayerEvent) obj;
		return (this.activePlayer == (that.activePlayer) && this.state == (that.state));							
	}
	
	/**
	 * Digests the data stored in an instance of the class into a single hash value (a 32-bit signed integer).
	 * Good practice to override along with equals.
	 */
	@Override
    public int hashCode() {
        return Objects.hash(PLAYING, WAITING, LEFTGAME, WON, state, activePlayer);
    }
}
