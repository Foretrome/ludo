package no.ntnu.imt3281.ludo.logic;

/**
 * Any class that implements the DiceListener interface must provide an implementation for a 
 * method named diceThrown that accepts a DiceEvent.
 * @author Tommy
 *
 */
public interface DiceListener {

	void diceThrown(DiceEvent diceEvent);

}
