package no.ntnu.imt3281.ludo.logic;

/**
 * Any class that implements the PieceListener interface must provide an implementation for a 
 * method named pieceMoved that accepts a PieceEvent.
 * @author Tommy
 *
 */
public interface PieceListener {

	void pieceMoved(PieceEvent pe);

}
