package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * Class to handle events caused by a moving piece. This class should always be called when a piece is moved in the game,
 * so an event is made. This enables the listeners of the PieceListener interface to receive these events
 * @author Tommy
 */
@SuppressWarnings("serial")
public class PieceEvent extends EventObject implements PieceListener {
	int player;
	int piece;
	int from;
	int to;

	/**
	 * Constructor for PieceEvent. Creates the EventObject
	 * @param ludoObject The object on which the Event initially occurred.
	 */
	public PieceEvent(Object ludoObject) {
		super(ludoObject);
	}

	/**
	 * Constructor for PieceEvent. Creates the EventObject and sets player, piece, where from and to.
	 * @param ludoObject The object on which the Event initially occurred.
	 * @param player The player who created the Event / moved the piece
	 * @param piece The piece that got moved
	 * @param from The position the piece got moved from
	 * @param to The position the piece got moved to
	 */
	public PieceEvent(Ludo ludoObject, int player, int piece, int from, int to) {
		super(ludoObject);
		setPlayer(player);
		setPiece(piece);
		setFrom(from);
		setTo(to);
	}
	
	/**
	 * Sets the player value of this object
	 * @param player The value to be set in player
	 */
	public void setPlayer(int player) {
		this.player = player;
	}

	/**
	 * Sets the piece value of this object
	 * @param piece The value to be set in piece
	 */
	public void setPiece(int piece) {
		this.piece = piece;
	}
	
	/**
	 * Sets the from value of this object
	 * @param from The value to be set in from
	 */
	public void setFrom(int from) {
		this.from = from;
	}
	
	/**
	 * Sets the to value of this object
	 * @param to The value to be set in to
	 */
	public void setTo(int to) {
		this.to = to;
	}
	
	/**
	 * Returns the player value of this object
	 * @return The player value
	 */
	public int getPlayer() {
		return player;
	}
	
	/**
	 * Returns the piece value of this object
	 * @return The piece value
	 */
	public int getPiece() {
		return piece;
	}
	
	/**
	 * Returns the from value of this object
	 * @return The from value
	 */
	public int getFrom() {
		return from;
	}
	
	/**
	 * Returns the to value of this object
	 * @return The to value
	 */
	public int getTo() {
		return to;
	}

	@Override
	public void pieceMoved(PieceEvent pe) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Compares two PieceEvent objects. Needed for the test to check order of events
	 * @param obj The object to compare to 'this'
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PieceEvent)) {
			return false;
		}
		PieceEvent that = (PieceEvent) obj;
		// Custom equality check
		return (this.player == (that.player) && this.piece == (that.piece)
				&& this.from == (that.from) && this.to == (that.to));							
	}
	
	/**
	 * Digests the data stored in an instance of the class into a single hash value (a 32-bit signed integer).
	 * Good practice to override along with equals.
	 */
	@Override
    public int hashCode() {
        return Objects.hash(player, piece, to, from);
    }
}
