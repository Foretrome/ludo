package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * Class to handle events caused by a roll of dice. This class should always be called when a dice is rolled in the game,
 * so an event is made. This enables the listeners of the DiceListener interface to receive these events
 * @author Tommy
 */
@SuppressWarnings("serial")
public class DiceEvent extends EventObject implements DiceListener {
	int player;
	int dice;

	/**
	 * Constructor for DiceEvent. Creates the EventObject
	 * @param ludoObject The object on which the Event initially occurred.
	 */
	public DiceEvent(Object ludoObject) {
		super(ludoObject);
	}

	/**
	 * Constructor for DiceEvent. Creates the EventObject and sets player and dice
	 * @param ludoObject The object on which the Event initially occurred.
	 * @param player The player who created the Event / rolled the dice
	 * @param dice The dice that was rolled, 1-6.
	 */
	public DiceEvent(Ludo ludoObject, int player, int dice) {
		super(ludoObject);
		setPlayer(player);
		setDice(dice);
	}

	/**
	 * Sets the player value of this object
	 * @param player The value to be set in player
	 */
	public void setPlayer(int player) {
		this.player = player;
	}
	
	/**
	 * Sets the dice value of this object
	 * @param dice The value to be set in dice
	 */
	public void setDice(int dice) {
		this.dice = dice;
	}
	
	/**
	 * Returns the player value of this object
	 * @return The player value
	 */
	public int getPlayer() {
		return player;
	}
	
	/**
	 * Returns the dice value of this object
	 * @return The dice value
	 */
	public int getDice() {
		return dice;
	}

	@Override
	public void diceThrown(DiceEvent diceEvent) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Compares two DiceEvent objects. Needed for the test to check order of events
	 * @param obj The object to compare to 'this'
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DiceEvent)) {
			return false;
		}
		DiceEvent that = (DiceEvent) obj;
		// Custom equality check
		return (this.player == (that.player) && this.dice == (that.dice));
	}
	
	/**
	 * Digests the data stored in an instance of the class into a single hash value (a 32-bit signed integer).
	 * Good practice to override along with equals.
	 */
	@Override
    public int hashCode() {
        return Objects.hash(player, dice);
    }
}
