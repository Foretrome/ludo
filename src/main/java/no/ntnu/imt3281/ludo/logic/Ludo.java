package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class represents a single game of Ludo including all it's players and associated logic
 * @author Tommy
 */
public class Ludo implements DiceListener, PieceListener, PlayerListener {

	/**
	 * Constant for the player red. Should always be player number zero.
	 */
	public static final int RED = 0;
	
	/**
	 * Constant for the player blue. Should always be player number one.
	 */
	public static final int BLUE = 1;
	
	/**
	 * Constant for the player yellow. Should always be player number two.
	 */
	public static final int YELLOW = 2;
	
	/**
	 * Constant for the player green. Should always be player number three.
	 */
	public static final int GREEN = 3;
	ArrayList<String> players;
	protected int activePlayer;
	int dice;
	Random randomGenerator;
	int[][] playerPieces;
	ArrayList<DiceListener> diceListeners = new ArrayList<>();
	ArrayList<PieceListener> pieceListeners = new ArrayList<>();
	ArrayList<PlayerListener> playerListeners = new ArrayList<>();
	int antRolls;

	/**
	 * Constructor for the class Ludo. Initializes a game based on X amount of players (2-4) and creates the needed variables for this.
	 * @param player1 This player will be Red
	 * @param player2 This player will be blue
	 * @param player3 This player will be yellow
	 * @param player4 This player will be green
	 * @throws NotEnoughPlayersException This exception will be thrown if the game is created with less than two players.
	 */
	public Ludo(String player1, String player2, String player3, String player4) {
		if (player1 != null && player2 != null)  {									// Need 2+ player to create a game
			players = new ArrayList<>();												// Creates the list of players in this game
			addPlayer(player1); 													// Add player to game
			addPlayer(player2);														// Add player to game
			addPlayer(player3);														// Add player to game
			addPlayer(player4);														// Add player to game
			activePlayer = RED;														// RED player should always start the game
			dice = 0;																// Reset dice
			antRolls = 0;
			playerPieces = new int[4][4];
			for (int player = 0; player < 4; player++) {							// Place all pieces at position 0
	            for (int piece = 0; piece < 4; piece++) {
	            	playerPieces[player][piece] = 0;
	            }
	        }
		 } else {																	// Less than 2 players
			 throw new NotEnoughPlayersException("Not enough players.");
		 }
	}

	/**
	 * Constructor for the class Ludo. Initializes an empty game with all it's associated variables
	 */
	public Ludo() {
		players = new ArrayList<>();													// Creates the list of players in this game
		activePlayer = RED;															// RED player should always start the game
		dice = 0;																	// Reset dice
		antRolls = 0;
		playerPieces = new int[4][4];
		for (int player = 0; player < 4; player++) {								// Place all pieces at position 0
            for (int piece = 0; piece < 4; piece++) {
            	playerPieces[player][piece] = 0;
            }
        }
	}

	/**
	 * Convert between player locations and board locations for a given player's piece.
	 * RED 		+15 [1, 52], -37 [53], 		+14 [54, 59]
	 * BLUE 	+28 [1, 39], -24 [40, 53], 	+20 [54, 59]
	 * YELLOW	+41 [1, 26], -11 [27, 53], 	+26 [54, 59]
	 * GREEN	+54 [1, 13], +02 [14, 53], 	+32 [54, 59]
	 * @param player The player whom the piece belongs to.
	 * @param playerPieceLocation The location of a piece from the player's perspective
	 * @return The actual placement on the board (1-91)
	 */
	public int userGridToLudoBoardGrid(int player, int playerPieceLocation) {
		int actualBoardPlacement = playerPieceLocation;
			switch (player) {
				case RED: {
					if (playerPieceLocation == 0) {									// Player is home
						actualBoardPlacement = 0;
						break;
					} else if (playerPieceLocation < 53) {
						actualBoardPlacement+=15;
						break;
					}  else if (playerPieceLocation == 53) {
						actualBoardPlacement-=37;
						break;
					} else {														// Home stretch
						actualBoardPlacement+=14;
						break;
					}
				}
				case BLUE: {
					if (playerPieceLocation == 0) {									// Player is home
						actualBoardPlacement = 4;
						break;
					} else if (playerPieceLocation <= 39) {
						actualBoardPlacement+=28;
						break;
					} else if (playerPieceLocation <= 53) {
						actualBoardPlacement-=24;
						break;
					} else {														// Home stretch
						actualBoardPlacement+=20;
						break;
					}
				}
				case YELLOW: {
					if (playerPieceLocation == 0) {									// Player is home
						actualBoardPlacement = 8;
						break;
					} else if (playerPieceLocation <= 26) {
						actualBoardPlacement+=41;
						break;
					} else if (playerPieceLocation <= 53) {
						actualBoardPlacement-=11;
						break;
					} else {														// Home stretch
						actualBoardPlacement+=26;
						break;
					}
				}
				case GREEN: {
					if (playerPieceLocation == 0) {									// Player is home
						actualBoardPlacement = 12;
						break;
					} else if (playerPieceLocation <= 13) {
						actualBoardPlacement+=54;
						break;
					} else if (playerPieceLocation <= 53) {
						actualBoardPlacement+=2;
						break;
					} else {														// Home stretch
						actualBoardPlacement+=32;
						break;
					}
				}
				default:
					break;
			}
		return actualBoardPlacement;
	}
	
	/**
	 * Calculates the numbers of players in the game. Strips any nulls from the Vector while counting.
	 * @return The actual number of players in this game.
	 */
	public int nrOfPlayers() {
		int count = 0;
		for (String player : players) {
			if (player != null) {
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Calculates the numbers of active players in the game. Strips away players marked as "Inactive".
	 * @return The number of active players in this game.
	 */
	public int activePlayers() {
		int antPlayers = nrOfPlayers();
		for (String player : players) {
			if (player.substring(0, 8).equals("Inactive")) {
				antPlayers--;
			}
		}
		return antPlayers;
	}
	
	/**
	 * Gets the name of a player on a given index.
	 * @param index The index of the player in Vector.
	 * @return The players name
	 */
	public String getPlayerName(int index) {
		return players.get(index);
	}
	
	/**
	 * Adds a new player to the game. If the game already has 4 players (full board), it will throw an exception.
	 * @param player The name of the player to be added to the game.
	 */
	public void addPlayer(String player) {
		if (nrOfPlayers() >= 4) {
			throw new NoRoomForMorePlayersException("No room fore more players.");
		}
		players.add(nrOfPlayers(), player);
	}
	
	/**
	 * Removes a player from the game by marking player as "Inactive"
	 * @param player The name of the player to be marked/removed.
	 */
	public void removePlayer(String player) {
		int index = players.indexOf(player);
		if(index == -1) {
			throw new NoSuchPlayerException("This player does not exist");
		}
		playerStateChanged(new PlayerEvent(this, index, PlayerEvent.LEFTGAME));						// Create and send event
		if (index == activePlayer) {																// If player is active
			nextPlayer();																			// next player
		}
		String inactivePlayer = "Inactive: " + player;												// Rename
		players.set(index, inactivePlayer);
	}
	
	/**
	 * Find the position for a given players given piece based on the players view.
	 * @param player The player to inspect
	 * @param piece The piece to locate
	 * @return The position where this players piece is in the grid
	 */
	public int getPosition(int player, int piece) {
		return playerPieces[player][piece];
	}
	
	/**
	 * Returns how many active players are left in the game.
	 * @return The amount of active players.
	 */
	public int activePlayer() {
		return activePlayer;
	}
	
	/**
	 * Generates a pseudorandom number between and including 1-6.
	 * @return The number generated / Dice roll value
	 */
	public int throwDice() {
		randomGenerator = new Random();
		dice = randomGenerator.nextInt(6) + 1;
		return dice;
	}
	
	/**
	 * This function takes a dice value and provides functionality based on the number of eyes.
	 * if all the players pieces are located at home, player will get 3 rolls
	 * Else if you can't move based on your dice, turn goes to next player
	 * Creates an event and notifies listeners when done.
	 * @param value The dice rolled
	 * @return The dice value
	 */
	public int throwDice(int value) {
		dice = value;
		antRolls++;
		diceThrown(new DiceEvent(this, activePlayer, value));									// Create event and Notify Listeners
		
		if (!allHome() && antRolls == 3 && value == 6) {										// If roll nr 3 is a six and not all home
			nextPlayer();																		// Not allowed to move
		} else if (allHome() && antRolls == 3 && value != 6 ) {									// If all home and roll nr 3 is not a six
			nextPlayer();																		// Not able to move from home
		} else if (!canMove()) {
			nextPlayer();
		} 
		return value;
	}
	
	/**
	 * Will set the current player (activePlayer) to the next in line. 
	 * If next in line is inactive, this person will be skipped.
	 */
	private void nextPlayer() {
		playerStateChanged(new PlayerEvent(this, activePlayer, PlayerEvent.WAITING));				// Create and send event
		antRolls = 0;
		do {
			if ((activePlayer + 1) == nrOfPlayers()) {
				activePlayer = 0;
			} else {
				activePlayer++;
			}
		} while (players.get(activePlayer).startsWith("Inactive"));
		playerStateChanged(new PlayerEvent(this, activePlayer, PlayerEvent.PLAYING));				// Create and send event
	}

	/**
	 * Checks whether or not all pieces for activePlayer is still home.
	 * @return True if all pieces are home, false if any is out.
	 */
	private boolean allHome() {
    	for (int piece = 0; piece < 4; piece++) {
        	if (playerPieces[activePlayer][piece] != 0) {
        		return false;
        	}
        }
    	return true;
	}

	/**
	 * Moves a player piece if possible.
	 * If the piece can move the moving piece for that player is updated and an event is triggered.
	 * @param player The player that is moving the piece.
	 * @param from What field the piece is currently standing.
	 * @param to What field the piece is moving to.
	 * @return Returns true if the player can move a piece or false if not.
	 */
	public boolean movePiece(int player, int from, int to) {
		 int moveThisPiece = -1;
		   
	        for (int piece = 3; piece >= 0; piece--) {
	            if (playerPieces[player][piece] == from) {					// Check if player has a piece on from-position
	                moveThisPiece = piece;
	            }
	        }
	        if (moveThisPiece == -1) {										// If no piece to move from that location, return false
	            return false;
	        }
	        if (!canMove()) {
	        	return false;
	        }
	        if ((to - from) == dice) {										// If no cheating with the dice
	            playerPieces[player][moveThisPiece] = to;					// Move the piece
	            pieceMoved(new PieceEvent(this, player, moveThisPiece, from, to));
	            if (getWinner() != -1) {									// Check if this was the winning move
	            	playerStateChanged(new PlayerEvent(this, getWinner(), PlayerEvent.WON));
	            } else if (dice != 6) {										// if no bonus roll
	            	nextPlayer();
	            }
	        } 
	        if ((from == 0) && (to == 1) && (dice == 6)) {					// If moving out of "home"
	        	playerPieces[player][moveThisPiece] = to;					// Move piece
	        	pieceMoved(new PieceEvent(this, player, moveThisPiece, from, to));
	            nextPlayer();
	        }
	        if (to == 0) {
	        	playerPieces[player][moveThisPiece] = 0;
	        	pieceMoved(new PieceEvent(this, player, moveThisPiece, from, to));
	        }
	        checkUnfortunateOpponent(player, to);							// Checks if any player is already on this position
	        return true;
	}
	
	/**
	 * Test if a piece can move or not.
	 * @return True if piece can move, false if it can't.
	 */
	protected boolean canMove() {
		if (dice < 1 || dice > 6) { // Terningkast må være 1 til 6
			return false;
		}
		for (int piece = 0; piece < 4; piece++) {
			if (blocked(activePlayer, playerPieces[activePlayer][piece], playerPieces[activePlayer][piece] + dice)) {
				return false;	
			}
		}
		for (int piece = 0; piece < 4; piece++) {
			int pos = getPosition(activePlayer, piece);
			if (pos != 59 && pos + dice > 59) {
				return false;
			}
		}
		return true;
	} 
	
	/**
	 * This function checks if the opponent(s) has 2 or more pieces in a tower on the given path.
	 * @param player The active player that are moving the piece.
	 * @param from The field where the piece is moving from.
	 * @param to The field where the piece is moving to.
	 * @return Returns true if it's 2 or more pieces in a field and false if it's not.
	 */
	private boolean blocked(int player, int from, int to) {
		int realFrom = userGridToLudoBoardGrid(player, from);									// Mapping board grid
		int realTo = userGridToLudoBoardGrid(player, to);										// Mapping board grid
		int pieceOn;
		
		for (int i = realFrom+1; i <= realTo; i++) {											// Looping the distance
			for (int j = 0; j <= nrOfPlayers(); j++) {											// Looping all players
				pieceOn = 0;																	// Reset any found piece 
				for (int piece = 0; piece < 4; piece++) {										// Looping pieces
					if (i == userGridToLudoBoardGrid(j, getPosition(j, piece)) && j != activePlayer && i > 15) {	// If find a piece on this location and not activePlayer or piece at home
						if (pieceOn == i) {														// Found a second piece			
							return true;
						} else {																// Register first piece
							pieceOn = i;	
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks if there is a piece already located in the field where a player is moving to
	 * if yes, kick that piece back to home if not own piece
	 * @param player The player that moves the piece
	 * @param to The location player is moving to
	 */
	private void checkUnfortunateOpponent(int player, int to) {
		int realTo = userGridToLudoBoardGrid(player, to);
		for (int i = 0; i < nrOfPlayers(); i++) {												// Looping all players
			for (int piece = 0; piece <= 3; piece++) {											// Looping all pieces
				int opponentPieceOnGrid = userGridToLudoBoardGrid(i, playerPieces[i][piece]);	// Player piece in real grid
				if (opponentPieceOnGrid == realTo && i != player) {								// If crash and not with own piece
					movePiece(i, playerPieces[i][piece], 0);									// Moving piece to home
				}
			}
		}
	}

	/**
	 * Checks the states of the player and returns the result (Created, Initiated, Started).
	 * @return Returns the status of a player.
	 */
	public String getStatus() {
		String status = null;
		if (nrOfPlayers() == 0) {
			status = "Created";
		} else if (getWinner() != -1) {
			status = "Finished";
		} else if (dice != 0 && getWinner() == -1) {
			status = "Started";
		} else {
			status = "Initiated";
		} 
		return status;
	}
	
	/**
	 * Checks if the game has a winner and returns the winner.
	 * @return Returns the winner if someone is finished, else will return -1.
	 */
	public int getWinner() {
		int winner = -1;
		for (int player = 0; player < 4; player++) { 
			if (playerPieces[player][0] == 59 && playerPieces[player][1] == 59 && playerPieces[player][2] == 59 && playerPieces[player][3] == 59) {
				winner = player;
			}
		}
		return winner;
	}
	
	/**
	 * Returns the position to all of the players pieces.
	 * 
	 * @param player The player that owns the pieces.
	 * @return Returns an array which holds the position of the pieces.
	 */
	public int[] getPlayerPieces(int player) {
		int[] piece = null;
		piece = new int[4];
		for (int i = 0; i < 4; i++) {
			piece[i] = playerPieces[player][i];
		}
		return piece;
	}
	
	/**
	 * Adds a new DiceListener to the Vector
	 * @param diceListener The listener to be added
	 */
	public void addDiceListener(DiceListener diceListener) {
		diceListeners.add(diceListener);
	}

	/**
	 * Adds a new PieceListener to the Vector
	 * @param pieceListener The listener to be added
	 */
	public void addPieceListener(PieceListener pieceListener) {
		pieceListeners.add(pieceListener);
	}

	/**
	 * Adds a new PlayerListener to the Vector
	 * @param playerListener The listener to be added
	 */
	public void addPlayerListener(PlayerListener playerListener) {
		playerListeners.add(playerListener);
	}

	/**
	 * Notifies all Listeners that a player state has changed
	 * @param playerEvent The event that has occurred.
	 */
	@Override
	public void playerStateChanged(PlayerEvent playerEvent) {
		for (int i = 0;  i < playerListeners.size();  i++)
			playerListeners.get(i).playerStateChanged(playerEvent);
	}

	/**
	 * Notifies all Listeners that a piece has been moved
	 * @param pieceEvent The event that has occurred
	 */
	@Override
	public void pieceMoved(PieceEvent pieceEvent) {
		for (int i = 0;  i < pieceListeners.size();  i++)
			pieceListeners.get(i).pieceMoved(pieceEvent);
	}

	/**
	 * Notifies all Listeners that a dice has been thrown
	 * @param diceEvent The event that has occurred
	 */
	@Override
	public void diceThrown(DiceEvent diceEvent) {
		for (int i = 0;  i < diceListeners.size();  i++)
			diceListeners.get(i).diceThrown(diceEvent);
	}
}
