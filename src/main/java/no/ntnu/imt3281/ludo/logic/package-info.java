/**
 * This package contains the logic behind a game of Ludo, with all it's dependencies.
 */
package no.ntnu.imt3281.ludo.logic;