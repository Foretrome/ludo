package no.ntnu.imt3281.ludo.logic;

/**
 * @author Foretrome
 *
 * This exception is thrown when 
 *
 */
@SuppressWarnings("serial")
public class NoSuchPlayerException extends RuntimeException {
	
	/**
	 * Constructs a new runtime exception with the specified detail message. 
	 * @param message the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method
	 */
	public NoSuchPlayerException(String message) {
		super(message);
	}

}
