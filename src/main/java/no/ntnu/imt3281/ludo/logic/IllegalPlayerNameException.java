package no.ntnu.imt3281.ludo.logic;

/**
 * This exception is thrown when someone tries to create a users with an illegal username
 * @author Tommy
 */
@SuppressWarnings("serial")
public class IllegalPlayerNameException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with the specified detail message. 
	 * @param message the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method
	 */
	public IllegalPlayerNameException(String message) {
		super(message);
	}
}
