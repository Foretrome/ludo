package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.client.ClientConnection;

public class LudoController {
	
	/**
     * Java.util Logger for handling exceptions and such
     */
    private static final Logger LOGGER = Logger.getLogger(LudoController.class.getName());
    ClientConnection cc;
    private LudoController ludoController;

    @FXML
    private MenuItem random;

    @FXML
    private TabPane tabbedPane;

    /**
     * When the user clicks on Random Game she/he is sent to a waiting queue
     * to wait for more players to join the game.
     * A controller for this class Ludocontroller is also sent to make sure the
     * player is sent to the gameboard when enough players has entered.
     * 
     * @param event Is not used.
     */
    @FXML
    public void joinRandomGame(ActionEvent event) {  	
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("WaitQueue.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
    	
    	WaitQueueController.clientConnection = cc;
    	/*
    	// Dummy code to hard test
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
		
    	String[] players = {"BJARNE", "MAX", null, null};
    	
    	try {
    		AnchorPane gameBoard = loader.load();
        	Tab tab = new Tab("Game");
    		tab.setContent(gameBoard);
        	tabbedPane.getTabs().add(tab);
        	GameBoardController controller = loader.getController();
        	controller.initializeGame(players);
    	} catch (IOException ex) {
    		LOGGER.log(Level.SEVERE, ex.toString(), ex);
		}
		*/
 
    	try {
    		AnchorPane waitQueue = loader.load();
	    	Tab tab = new Tab("Wait Queue");
			tab.setContent(waitQueue);
	    	tabbedPane.getTabs().add(tab);
	    	WaitQueueController controller = loader.getController();
	    	controller.initialize(ludoController);
    	} catch (IOException e) {
    		LOGGER.log(Level.WARNING, "Failed to join queue" + e.toString(), e);
    	}
    }
    
    /**
     * Will load the profile page and it's controller
     * @param e not used
     */
    @FXML
    public void profilePage(ActionEvent e) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfilePage.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
    	
    	ProfilePageController.clientConnection = cc;
    	
    	try {
	    	AnchorPane profilePage = loader.load();
	    	Tab tab = new Tab("Profile Page");
			tab.setContent(profilePage);
	    	tabbedPane.getTabs().add(tab);
    	} catch (IOException ex) {
    		LOGGER.log(Level.SEVERE, ex.toString(), ex);
		}
    }
    
    /**
     * Will load the Global chat and it's controller
     * @param event not used
     */
    @FXML
    public void globalChat(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("GlobalChat.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
    	
    	GlobalChatController.clientConnection = cc;
    	
    	try {
	    	Pane globalChatBoard = loader.load();
	    	Tab tab = new Tab("Global Chat");
			tab.setContent(globalChatBoard);
	    	tabbedPane.getTabs().add(tab);
    	} catch (IOException ex) {
    		LOGGER.log(Level.SEVERE, ex.toString(), ex);
		}
    }
    
    /**
     * Function to exit the application itself. Is currently called when a uses clicks 'close'
     * @param event not used
     */
    @FXML
    void exitLudo(ActionEvent event) {
    	// TODO Leave all games, send LOGOUT message
    	System.exit(0);
    }
    
    /**
     * Will load the Highscores tab and it's controller
     * @param event not used
     */
    @FXML
    void viewHighscores(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("HighScores.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
    	
    	try {
	    	Pane highScores = loader.load();
	    	Tab tab = new Tab("HighScores");
			tab.setContent(highScores);
	    	tabbedPane.getTabs().add(tab);
    	} catch (IOException ex) {
    		LOGGER.log(Level.SEVERE, ex.toString(), ex);
		}
    }

    /**
     * Sets the ClientConnection both on this Controller and on the actual Client object
     * @param cc The ClientConnection to link
     */
	public void setClient(ClientConnection cc) {
		this.cc = cc;
		Client.SetCC(cc);
	}

	/**
	 * After 2 or more players have joined and 30 seconds have passed, the players will
	 * be sent to the gameboard.
	 * @param players sends the players in the current game.
	 */
	public void joinGame(String[] players) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
		
    	try {
    		AnchorPane gameBoard = loader.load();
        	Tab tab = new Tab("Game");
    		tab.setContent(gameBoard);
        	tabbedPane.getTabs().add(tab);
        	GameBoardController controller = loader.getController();
        	controller.initializeGame(players);
    	} catch (IOException ex) {
    		LOGGER.log(Level.SEVERE, ex.toString(), ex);
		}
	}
}
