package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import no.ntnu.imt3281.ludo.client.ClientConnection;

/**
 * Controller for the Profile Page. Operations in this class is used to 
 * display a users data and gives user the opportunity to change this data.
 * @author Tommy
 *
 */
public class ProfilePageController {
	
	/**
	 * The link between client and it's socket to the server
	 */
	public static ClientConnection clientConnection;
	
	/**
     * Java.util Logger for handling exceptions and such
     */
	private static final Logger LOGGER = Logger.getLogger(ProfilePageController.class.getName());
    private String fname;
    private String lname;
    private String email;

    @FXML
    private Label labelUsername;

    @FXML
    private TextField textfieldUsername;

    @FXML
    private Label labelPassword;

    @FXML
    private TextField textfieldPassword;

    @FXML
    private Label labelFirstname;

    @FXML
    private TextField textfieldFirstname;

    @FXML
    private Label labelLastname;

    @FXML
    private TextField textfieldLastname;

    @FXML
    private Label labelEmail;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private Button saveButton;

    /**
     * Event for saving the changes done on a users profile page. This event is triggered when the user clicks the saveButton.
     * Event will send separate querys to the server based on which data is changed.
     * @param event The button click
     */
    @FXML
    void saveProfileChanges(ActionEvent event) {
		try {
			// Opens streams and sends the request for user data to server
			BufferedWriter output = new BufferedWriter(new OutputStreamWriter(clientConnection.getConnection().getOutputStream()));
	    	
			// Get values from textFields
	    	String firstname = textfieldFirstname.getText().trim();
			String lastname = textfieldLastname.getText().trim();
			String emailaddress = textFieldEmail.getText().trim();
			String password = textfieldPassword.getText();
			
			// Check if textFields have new values and write to stream if they do
			if (isValueUpdated("firstname", firstname)) {
				//UPDATEPROFILE:FIELD:VALUE
				String str = "UPDATEPROFILE>" + "firstname>" + firstname;
				output.write(str);
				output.newLine();
			}
			
			if (isValueUpdated("lastname", lastname)) {
				//UPDATEPROFILE:FIELD:VALUE
				String str = "UPDATEPROFILE>" + "lastname>" + lastname;
				output.write(str);
				output.newLine();
			}
			
			if (isValueUpdated("emailaddress", emailaddress)) {
				//UPDATEPROFILE:FIELD:VALUE
				String str = "UPDATEPROFILE>" + "emailaddress>" + emailaddress;
				output.write(str);
				output.newLine();
			}
			
			if (password.length() > 2) {
				//UPDATEPROFILE:FIELD:VALUE
				String str = "UPDATEPROFILE>" + "password>" + password;
				output.write(str);
				output.newLine();
			}
			// Send to server
			output.flush();
			initialize();		// Reload data after updates. Swap this with a Tab close eventually
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Failed to update profile" + e.toString(), e);
		}
    }
    
    /**
     * Checks if the value in any given field has been changed since it was loaded
     * @param field The field to check if changed
     * @param newValue The value currently in that field
     * @return Will be true if the field has a changed value
     */
    private boolean isValueUpdated(String field, String newValue) {
    	String oldValue = null;
    	switch (field) {
			case "firstname": oldValue = fname; break;
			case "lastname": oldValue = lname; break;
			case "emailaddress": oldValue = email; break;
			case "password": break;
			default: break;
    	}
    	if (!newValue.equals(oldValue) && oldValue!=null) {
    		return true;
    	}
    	return false;
	}

	/**
     * Initializes the Profile page tab. During initialization, a request for user data will be created and sent to the server,
     * once the server replies, the user data will be displayed in editable fields.
     */
    @FXML
    public void initialize() {
		String str = "GETPROFILE";
		try {
			// Opens streams and sends the request for user data to server
			BufferedWriter output = new BufferedWriter(new OutputStreamWriter(clientConnection.getConnection().getOutputStream()));
			BufferedReader input = new BufferedReader(new InputStreamReader(clientConnection.getConnection().getInputStream()));
			output.write(str);
			output.newLine();
			output.flush();
			LOGGER.log(Level.INFO, "Requested profile data.", clientConnection.getToken());
			
			// Reads the server response
			String[] content = getContentFromServer(input);
			
			// Display content
            String username = content[2];
            String firstname = content[3];
            String lastname = content[4];
            String emailaddress = content[5];
            
            // Save data global to compare if changed later
            fname = firstname;
            lname = lastname;
            email = emailaddress;
                 
            // Not allowed to change username
            textfieldUsername.setText(username);
     		textfieldUsername.setEditable(false);
     		 
     		// Not showing actual password
     		textfieldPassword.setPromptText("*********");
     
         	textfieldFirstname.setText(firstname);
         	textfieldLastname.setText(lastname);
         	textFieldEmail.setText(emailaddress);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.toString(), e);
		}
    }

    /**
     * Reads the server response in input, splits the content into an array and replaces any 'null' with blanks.
     * @param input The stream to read from
     * @return The content array
     * @throws IOException Any exception thrown
     */
	private String[] getContentFromServer(BufferedReader input) throws IOException {
		String[] content = null;
		String line = input.readLine();
		if (line.startsWith("PROFILE")) {
			// READ Data
			content = line.split(":");
			
			// Replace any null values with blanks in view
			for (int i = 0; i < content.length; i++) {
				if (content[i].equals("null")||content[i].equals("")) {
					content[i] = "";
				}
			}
		}
		return content;
	}
}