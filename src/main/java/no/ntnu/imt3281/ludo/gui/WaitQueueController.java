	/**
 * 
 */
package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import no.ntnu.imt3281.ludo.client.ClientConnection;

/**
 * @author Foretrome
 *
 */
public class WaitQueueController {
	
	/**
     * Java.util Logger for handling exceptions and such
     */
	private static final Logger LOGGER = Logger.getLogger(ProfilePageController.class.getName());
	
	/**
	 * Connection to the client.
	 */
	public static ClientConnection clientConnection;
	private LudoController ludo;
	
	// Title for waiting queue.
	@FXML
    private Label waitQueue;

	// Player 1.
    @FXML
    private Label player1;

    // Player 2.
    @FXML
    private Label player2;

    // Player 3.
    @FXML
    private Label player3;

    // Player 4.
    @FXML
    private Label player4;

    // Indicator to show the user that gives user feedback.
    @FXML
    private ProgressIndicator progressIndicator;
    
    /**
     * Initialize the waiting queue tab. When players click the Random Game button
     * they are added to a queue on the server and on the screen while waiting for the game to start.
     * 
     * @param ludoController gets a LudoController to start joinGame.
     */
    public void initialize(LudoController ludoController) {
    	
    	String str = "JOINWAITINGQUEUE";
    	String str2 = "STATUSQUEUE";
    	
    	ludo = ludoController;
    	
    	try {
		    BufferedWriter output = new BufferedWriter(new OutputStreamWriter(clientConnection.getConnection().getOutputStream()));
			BufferedReader input = new BufferedReader(new InputStreamReader(clientConnection.getConnection().getInputStream()));
			
			output.write(str);
			output.newLine();
			output.flush();
			output.write(str2);
			output.newLine();
			output.flush();
			
			if (input.ready()) {
			System.out.println(input.readLine());
				String playersInQueue = input.readLine();
				String[] content = playersInQueue.split(">");
				String player = content[1];
				String removedBrackets = player.substring(1, player.length() - 1);
				String[] players = removedBrackets.split(",");
				
				//if (players.length >= 2 && players.length <= 4) {
				
					// A task thread that loops for 30 seconds and adds player names to the screen
					// as they join the random game. When enough players have joined, the game starts.
					Task<Integer> task = new Task<Integer>() {
						@Override
						protected Integer call() throws Exception {	
							long time = System.currentTimeMillis();
							while (System.currentTimeMillis() - time < 3000) {
								for (int i = 0; i < players.length; i++) {
									if (i == 0) {
										player1.setText(players[i]);
									}
									if (i == 1) {
										player2.setText(players[i]);
									}
									if (i == 2) {
										player3.setText(players[i]);
									}
									if (i == 3) {
										player4.setText(players[i]);
										Platform.runLater(() -> ludo.joinGame(players));
									}
								}
						    	Thread.sleep(1000);
						    	Platform.runLater(() -> progressIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS));
							}
							Platform.runLater(() -> ludo.joinGame(players));
							return null;
						}
					};
					
					Thread t = new Thread(task);
					t.start();
			}
			LOGGER.log(Level.INFO, "Requested waiting queue data.", clientConnection.getToken());
			//}
    	} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.toString(), e);
		}
    } 
}