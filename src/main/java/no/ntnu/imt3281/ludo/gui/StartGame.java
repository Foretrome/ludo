package no.ntnu.imt3281.ludo.gui;

import java.util.ArrayList;

import no.ntnu.imt3281.ludo.logic.Ludo;

public class StartGame extends Ludo {
	
	int gameId;
	ArrayList<String> playersInGame = new ArrayList<String>();
	
	/**
	 * Initializes the new game with the players and an ID for that game.
	 * @param players is the players to be added to the ludogame.
	 */
	public StartGame(String[] players) {
		
		Ludo ludo = new Ludo(players[0], players[1] , players[2], players[3]);
		
		++gameId;
		
		for (int i = 0; i < ludo.nrOfPlayers(); i++) {
			playersInGame.add(ludo.getPlayerName(i));
		}
	}

	/**
	 * Returns the gameId unique for a game.
	 * 
	 * @return gameId Returns the id this game.
	 */
	public int getGameCode() {
		return gameId;
	}

	/**
	 * Returns the names of the players that are in this game.
	 * 
	 * @return playersInGame Returns player names.
	 */
	public ArrayList<String> getPlayerNames() {
		return playersInGame;
	}
	
}
