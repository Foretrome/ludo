package no.ntnu.imt3281.ludo.gui;

/**
 * 
 * @author Foretrome
 *
 */
public class ThrowDice {

	/**
	 * The ID of a game.
	 */
	public int gameId;
	
	/**
	 * Instantiates the ID for a game.
	 * 
	 * @param gameId the ID of the game.
	 */
	public ThrowDice(int gameId) {
		this.gameId = gameId;
	}
	
	/**
	 * Returns an ID for a certain game.
	 * 
	 * @return gameId is the id to be returned.
	 */
	public int getGameId() {
		return gameId;
	}
}
