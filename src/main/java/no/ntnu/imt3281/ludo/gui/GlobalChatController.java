package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import no.ntnu.imt3281.ludo.client.ClientConnection;

public class GlobalChatController {

	/**
	 * Java.util Logger for handling exceptions and such
	 */
	private static final Logger LOGGER = Logger.getLogger(GlobalChatController.class.getName());
	public static ClientConnection clientConnection;

	@FXML
	private Label globalchatHeader;

	@FXML
    private TextArea globalchatTextareaTextbox;

	@FXML
	private TextField globalchatTextfieldInput;

	@FXML
	private Button globalchatButtonSend;

	/**
	 * This function is fired when the 'Send' button is pressed.
	 * Will take any message in the input field and send it to the server for processing
	 * @param event button event
	 */
	@FXML
	void sendMessage(ActionEvent event) {
		String message = globalchatTextfieldInput.getText().trim();
		if (!message.equals("")) {
			try {
				// Opens streams
				BufferedWriter output = new BufferedWriter(new OutputStreamWriter(clientConnection.getConnection().getOutputStream()));

				// Send message to server
				String str = "GLOBAL>" + message;
				output.write(str);
				output.newLine();
				output.flush();
			}  catch (IOException e) {
				LOGGER.log(Level.WARNING, "Failed to send global message" + e.toString(), e);
			}
		}
		// Clear input field after action
		globalchatTextfieldInput.clear();
	}

	/**
	 * Initializes the Global chat tab
	 */
	@FXML
	public void initialize() {
		//Adds a OnKeyPressed listener on the input-field to make sure "Enter" will also send the message
		globalchatTextfieldInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					sendMessage(null);
				}
			}
		});
	}
}
