package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.client.ClientConnection;
import no.ntnu.imt3281.ludo.logic.IllegalPlayerNameException;

/**
 * Controller for handling the GUI part of the login process. Handles logic for both Login and Registration for players
 * @author Tommy
 */
public class LoginScreenController {
	
	/**
     * Java.util Logger for handling exceptions and such
     */
    private static final Logger LOGGER = Logger.getLogger(LoginScreenController.class.getName());
    ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
    
    // fx:id="loginUser"
    @FXML
    private TextField loginUser;

    // fx:id="loginPassword"
    @FXML
    private TextField loginPassword;

    // fx:id="newUsername"
    @FXML 
    private TextField newUsername;
    
    // fx:id="newPassword"
    @FXML 
    private TextField newPassword;

    /**
     * Will send a packet to the server for registration if both password and username is okay
     * @param event The event from the button to register
     * @throws SQLException Any exception when dealing with the DB
     */
	@FXML
    void createNewUserAccount(ActionEvent event) throws SQLException {
    	if (validateNewUserInfo()) {
    		String user = newUsername.getText().toLowerCase().trim();
    		String hash = hashPassword(newPassword.getText());
			try(Socket connection = new Socket(InetAddress.getLocalHost().getHostName(), 9876)
				){
				
				// Create and send String to server
				String str = "REGISTER:" + user + ":" + hash;
				BufferedWriter output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
				BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				output.write(str);
				output.newLine();
				output.flush();
				
				// Retrieve response from server
				String line = input.readLine();
	            if (line.equals("REGISTERED")) {
	            	LOGGER.log(Level.INFO, "User registered OK. Closing connection.");
	            } else {
	            	LOGGER.log(Level.WARNING, bundle.getString("system.registratonFailed"));
	            }
	            connection.close();
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.toString(), e);
			}
	    }
    }

    /**
     * Hashes the password with SHA-1
     * @param password The password to be hashed
     * @return The finished hash
     */
    public static String hashPassword(String password) {
    	String hash = null;
	    try {
	        MessageDigest digest = MessageDigest.getInstance("SHA-1");
	        digest.update(password.getBytes(), 0, password.length());
	        hash = new BigInteger(1, digest.digest()).toString(16);
	    } catch (NoSuchAlgorithmException e) {
	    	LOGGER.log(Level.SEVERE, e.toString(), e);
	    }
		return hash;
	}

	/**
     * Opens a connection with the server, and attempts to log in with the given username/password.
     * If the login is correct, a token will be received
     * @param event The event from the button to login
     */
	@FXML
    void login(ActionEvent event) {
    	String user = loginUser.getText().toLowerCase().trim();
		String pwd = hashPassword(loginPassword.getText());
    	try{
    		Socket serverSocket = new Socket(InetAddress.getLocalHost().getHostName(), 9876);
			BufferedReader input = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
			BufferedWriter output = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()));
			
			// Create and send String to server
			String str = "LOGIN:" + user + ":" + pwd;				
			output.write(str);
			output.newLine();
			output.flush();
			
			// Retrieve response from server
			String line = input.readLine();
            String[] content = line.split(":");
            String operation = content[0];
            String token = content[1];
            
            // If logged in and recieved token, open Ludo GUI
            if (operation.equals("LOGGEDIN") && !token.equals("")) {
            	LOGGER.log(Level.INFO, "Client recieved token");
            	// New connection object
            	ClientConnection cc = new ClientConnection(serverSocket, token);
            	
            	// Opens Ludo GUI
            	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../gui/Ludo.fxml"), bundle);

            	Parent root = (Parent)fxmlLoader.load();
            	LudoController controller = fxmlLoader.<LudoController>getController();
            	
            	// Send ClientConnection to Parent object
            	controller.setClient(cc);
            	Scene scene = new Scene(root); 
            	
            	//Here is the magic. We get the reference to main Stage.
            	Stage mainWindow;
            	mainWindow = (Stage)  ((Node)event.getSource()).getScene().getWindow();
    		    mainWindow.setScene(scene);
    		    mainWindow.show();
            } else {
            	LOGGER.log(Level.WARNING, bundle.getString("system.loginFailed"));
            }
		} catch (IOException e1) {
			LOGGER.log(Level.SEVERE, e1.toString(), e1);
		}
    }

	/**
     * Validates the username and/or password for a user that wants to register a new account
     * @return True if information is approved
     */
    boolean validateNewUserInfo() {
    	int minLength = 2;
    	// Username starts with **** will throw an exception
    	if (newUsername.getText().startsWith("****")) {
    		throw new IllegalPlayerNameException("Illegal player name");
    	}
    	// Too short username
    	if (newUsername.getText().length() < minLength) {
    		LOGGER.log(Level.WARNING, bundle.getString("system.tooShortUsername"));
    		return false;
    	}
    	// Too short password
    	if (newPassword.getText().length() < minLength) {
    		LOGGER.log(Level.WARNING, bundle.getString("system.tooShortPassword"));
    		return false;
    	}
    	return true;
    }
}
