package no.ntnu.imt3281.ludo.gui;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Controller for the HighScores page.
 * Will eventually find the top 10 players who either won or just played most games from the database
 * @author Tommy
 *
 */
public class HighScoresController {

    @FXML
    private TextField mostWonFirst;

    @FXML
    private TextField mostWonSecond;

    @FXML
    private TextField mostWonThird;

    @FXML
    private TextField mostWonFourth;

    @FXML
    private TextField mostWonFifth;

    @FXML
    private TextField mostWonSixth;

    @FXML
    private TextField mostWonSeventh;

    @FXML
    private TextField mostWonEight;

    @FXML
    private TextField mostWonNinth;

    @FXML
    private TextField mostWonTenth;

    @FXML
    private TextField mostPlayedFirst;

    @FXML
    private TextField mostPlayedSecond;

    @FXML
    private TextField mostPlayedThird;

    @FXML
    private TextField mostPlayedFourth;

    @FXML
    private TextField mostPlayedFifth;

    @FXML
    private TextField mostPlayedSixth;

    @FXML
    private TextField mostPlayedSeventh;

    @FXML
    private TextField mostPlayedEigth;

    @FXML
    private TextField mostPlayedNinth;

    @FXML
    private TextField mostPlayedTenth;

}
