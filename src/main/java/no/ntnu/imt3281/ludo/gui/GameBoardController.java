package no.ntnu.imt3281.ludo.gui;

import java.awt.Point;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;

/**
 * Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;
import no.ntnu.imt3281.ludo.server.ServerConnection;

/**
 * @author Foretrome, Oyvind Kolloen
 */

public class GameBoardController extends Ludo {

	@FXML
    private AnchorPane boardPane;

	@FXML
	private ImageView move;
	
    @FXML
    private Label player1Name;

    @FXML
    private ImageView player1Active;

    @FXML
    private Label player2Name;

    @FXML
    private ImageView player2Active;

    @FXML
    private Label player3Name;
    
    @FXML
    private ImageView player3Active;

    @FXML
    private Label player4Name;

    @FXML
    private ImageView player4Active;
    
    @FXML
    private ImageView diceThrown;
    
    @FXML
    private Button throwTheDice;

    @FXML
    private TextArea chatArea;

    @FXML
    private TextField textToSay;

    @FXML
    private Button sendTextButton;
    
    private PlayerEvent playerEvent;
    private int moveUserPieceFrom = -1;
    private int gameId;
    private int movingPlayer = RED;
    private int diceValue = 0;
    private Image playerPieceImages[] = new Image[4];
    private Rectangle moveTo = new Rectangle(46, 46);
    private Rectangle playerPieces[][] = new Rectangle[4][4];
    private TopLeftCorners corners = new TopLeftCorners();
    ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
    
    /**
     * Initializes the gameboard with a gameId and the current players for this game.
     * 
     * @param gameInfo returns the gameId and the name of the players.
     */
    public void initializeGame(StartGame gameInfo) {
    	
    	/**
    	 * Adds a unique ID for this game.
    	 */
    	this.gameId = gameInfo.getGameCode();
    	
    	/**
    	 * Adds all the players for this game.
    	 */
    	for (String playerName : gameInfo.getPlayerNames()) {
    		addPlayer(playerName);
    	}
    	
    	/**
    	 * Add player names to their place on the board.
    	 */
    	for (int i = 0; i < nrOfPlayers(); i++) {
    		if (i == 0) {
	    	player1Name.setText(getPlayerName(i));
    		}
    		if (i == 1) {
	    	player2Name.setText(getPlayerName(i));
    		}
    		if (i == 2) {
	    	player3Name.setText(getPlayerName(i));
    		} else {
    			player3Name.setText("");
    		}
    		if (i == 3) {
	    	player4Name.setText(getPlayerName(i));
    		} else {
    			player4Name.setText("");
    		}
		}
    	
    	if (movingPlayer == activePlayer) {
    		throwTheDice.setDisable(false);
    	}
    	
    	addDiceListener(de->{
    		Platform.runLater(()->diceThrown.setImage(new Image(getClass().getResourceAsStream("/images/dice"+de+".png"))));
    	});
    	addPlayerListener(pe->{
    		Platform.runLater(()->playerChange(pe.getActivePlayer(), pe.getState()));
    	});
    	
    	playerPieceImages[0] = new Image(getClass().getResourceAsStream("/images/red.png"));
    	playerPieceImages[1] = new Image(getClass().getResourceAsStream("/images/blue.png"));
    	playerPieceImages[2] = new Image(getClass().getResourceAsStream("/images/yellow.png"));
    	playerPieceImages[3] = new Image(getClass().getResourceAsStream("/images/green.png"));
    	
    	for (int player=0; player<4; player++) {
    		for (int piece=0; piece<4; piece++) {
    			playerPieces[player][piece] = new Rectangle(48,48);
    			playerPieces[player][piece].setFill(new ImagePattern(playerPieceImages[player]));
    			playerPieces[player][piece].setX(corners.point[player*4+piece].getX()/*-8+piece*4*/);
    			playerPieces[player][piece].setY(corners.point[player*4+piece].getY()/*-2+piece*2*/);
    			playerPieces[player][piece].setOnMouseClicked(e->handleMouseClick(e));
    			boardPane.getChildren().add(playerPieces[player][piece]);
    		}
    	}
    	
    	moveTo.setFill(new ImagePattern(new Image(getClass().getResourceAsStream("/images/selectedgrid.png"))));
    	moveTo.setX(-100);
    	moveTo.setY(-100);
    	boardPane.getChildren().add(moveTo);
    	moveTo.setOnMouseClicked(e->movePiece(e));
    	
    }

    /**
     * When a used clicks on a piece it's from position is fetched.
     * Then a check for whether that piece can move or not is done.
     * If the piece is at home it is moved to it's start position.
     * If the piece is out on the board it is move as many fields as the dice value is.
     * 
     * @param event is not used.
     */
    @FXML
    void handleMouseClick(MouseEvent event) {
    	moveUserPieceFrom = movePieceFrom(event);
    	if (moveUserPieceFrom > -1) {
			if (canMove()) {
		    	if (moveUserPieceFrom == 0) {
					moveTo.setX(corners.point[userGridToLudoBoardGrid(movingPlayer, 1)].getX());
					moveTo.setY(corners.point[userGridToLudoBoardGrid(movingPlayer, 1)].getY());
				} else {
					moveTo.setX(corners.point[userGridToLudoBoardGrid(movingPlayer, moveUserPieceFrom + diceValue)].getX());
					moveTo.setY(corners.point[userGridToLudoBoardGrid(movingPlayer, moveUserPieceFrom + diceValue)].getY());
				}
			} 
    	}
    }
    
    /**
     * Checks where the pieces of the current player is positioned.
     * 
     * @param event is not used.
     * @return fromPosition Returns the position of all the current players pieces.
     */
    private int movePieceFrom(MouseEvent event) {
    	int fromPosition = -1;
    	Object o = event.getSource();
    	for (int i = 0; i < 4; i++) {
    		if (o.equals(playerPieces[movingPlayer][i])) {
    			fromPosition = getPlayerPieces(movingPlayer)[i];
    		}
    	}
		return fromPosition;
    }
    
    /**
     * Called when the user is moving a piece.
     * 
     * @param event is not used.
     */
    private void movePiece(MouseEvent event) {
    	updateBoard();
    	ServerConnection.sendObjectToAllClients(new MovePiece(gameId, movingPlayer, moveUserPieceFrom, moveUserPieceFrom == 0 && diceValue == 6));
    }
    
    /**
     * Updates the gameboard with all the players piece position.
     */
    public void updateBoard() {
    	moveTo.setX(-100);
		moveTo.setY(-100);
    	for (int player = 0; player < 4; player++) {		// Place all pieces on the correct position
    		int pieces[] = getPlayerPieces(movingPlayer);
    		int piecesHome = 0;
    		for (int piece = 0; piece < 4; piece++) {
    			if (pieces[piece] == piecesHome) {
    				playerPieces[player][piece].setX(corners.point[userGridToLudoBoardGrid(player, 1)].getX());
    				playerPieces[player][piece].setY(corners.point[userGridToLudoBoardGrid(player, 1)].getY());
    			} else {
    				playerPieces[player][piece].setX(corners.point[userGridToLudoBoardGrid(player, pieces[piece] + diceValue)].getX());
    				playerPieces[player][piece].setY(corners.point[userGridToLudoBoardGrid(player, pieces[piece] + diceValue)].getY());
    			}
    		}	
    	}
    }
    
    /**
     * Called when a PlayerEvent is received from via the PlayerListener
     * Will be called whenever a player state changes (PLAYING, WAITING, LEFTGAME, WON)
     * 
     * @param player the player that changed state
     * @param state the new state of the player
     */
    public void playerChange(int player, int state) {
    	switch (state) {
	    	case PlayerEvent.WAITING: {
	    		changePlayerState(player, true);
	    		break;
	    	}
	    	case PlayerEvent.PLAYING: {
	    		diceThrown.setImage(new Image(getClass().getResourceAsStream("/images/rolldice.png")));
	    		changePlayerState(player, false);
	    		break;
	    	}
	    	case PlayerEvent.LEFTGAME: {
	    		switch (player) {			// What player left the game (set an (X) icon on that player)
	    		case RED:
	    			player1Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
	    			break;
	    		case BLUE:
	    			player2Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
	    			break;
	    		case YELLOW:
	    			player3Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
	    			break;
	    		case GREEN:
	    			player4Active.setImage(new Image(getClass().getResourceAsStream("/images/remove.png")));
	    			break;
	    		}
	    		break;	    		
	    	}
	    	case PlayerEvent.WON: {
	    		throwTheDice.setDisable(true);
	    		diceThrown.setImage(new Image(getClass().getResourceAsStream("/images/rolldice.png")));
	    		switch (player) {
	    		case RED:
	    			player1Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
	    			player1Active.setVisible(true);
	    			break;
	    		case BLUE:
	    			player2Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
	    			player2Active.setVisible(true);
	    			break;
	    		case YELLOW:
	    			player3Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
	    			player3Active.setVisible(true);
	    			break;
	    		case GREEN:
	    			player4Active.setImage(new Image(getClass().getResourceAsStream("/images/crown.png")));
	    			player4Active.setVisible(true);
	    			break;
	    		}
	    		break;
	    	}
    	}
    }
    
    /**
     * Changes the state of a player depending on if she/he is playing or waiting.
     * 
     * @param player The player changing state.
     * @param b True or False.
     */
    private void changePlayerState(int player, boolean b) {
    	playerEvent.setActivePlayer(player);
    	if (b == false) {
    	playerEvent.setState(0);
    	} else if (b == true) {
    		playerEvent.setState(1);
    	}
	}

    /**
     * 
     * @author Foretrome
     *
     * A class that creates corner points for all pieces to be positioned at
     * around the board.
     */
	public class TopLeftCorners {
    	Point point[] = new Point[92];
    	
    	/**
    	 * Set all locations
    	 */
    	public TopLeftCorners() {
    		for (int i = 0; i < point.length; i++) {
    			point[i] = new Point();
    		}
    		
    		// RED start fields
    		point[0].setLocation(554, 74);
    		point[1].setLocation(554+48, 74+48);
    		point[2].setLocation(554, 74+48*2);
    		point[3].setLocation(554-48, 74+48);
    		
    		// BLUE start fields
    		point[4].setLocation(554, 506);
    		point[5].setLocation(554+48, 506+48);
    		point[6].setLocation(554, 506+48*2);
    		point[7].setLocation(554-48, 506+48);
    		
    		// YELLOW start fields
    		point[8].setLocation(122, 506);
    		point[9].setLocation(122+48, 506+48);
    		point[10].setLocation(122, 506+48*2);
    		point[11].setLocation(122-48, 506+48);
    		
    		// GREEN start fields
    		point[12].setLocation(122, 74);
    		point[13].setLocation(122+48, 74+48);
    		point[14].setLocation(122, 74+48*2);
    		point[15].setLocation(122-48, 74+48);
    		
    		// Board 16-20. Red area: start.
    		point[16].setLocation(386, 50);
    		point[17].setLocation(386, 50+48);
    		point[18].setLocation(386, 50+48*2);
    		point[19].setLocation(386, 50+48*3);
    		point[20].setLocation(386, 50+48*4);
    		
    		// Board 21-33. Blue area.
    		point[21].setLocation(434, 290);
    		point[22].setLocation(434+48, 290);
    		point[23].setLocation(434+48*2, 290);
    		point[24].setLocation(434+48*3, 290);
    		point[25].setLocation(434+48*4, 290);
    		point[26].setLocation(434+48*5, 290);
    		point[27].setLocation(674, 290+48);
    		point[28].setLocation(674, 290+48*2);
    		point[29].setLocation(626, 386);
    		point[30].setLocation(578, 386);
    		point[31].setLocation(530, 386);
    		point[32].setLocation(482, 386);
    		point[33].setLocation(434, 386);
    		
    		// Board 34-46. Yellow area.
    		point[34].setLocation(386, 434);
    		point[35].setLocation(386, 482);
    		point[36].setLocation(386, 530);
    		point[37].setLocation(386, 578);
    		point[38].setLocation(386, 626);
    		point[39].setLocation(386, 674);
    		point[40].setLocation(338, 674);
    		point[41].setLocation(290, 674);
    		point[42].setLocation(290, 626);
    		point[43].setLocation(290, 578);
    		point[44].setLocation(290, 530);
    		point[45].setLocation(290, 482);
    		point[46].setLocation(290, 434);
    		
    		// Board 47-59. Green area.
    		point[47].setLocation(242, 386);
    		point[48].setLocation(194, 386);
    		point[49].setLocation(146, 386);
    		point[50].setLocation(98, 386);
    		point[51].setLocation(50, 386);
    		point[52].setLocation(2, 386);
    		point[53].setLocation(2, 338);
    		point[54].setLocation(2, 290);
    		point[55].setLocation(50, 290);
    		point[56].setLocation(98, 290);
    		point[57].setLocation(146, 290);
    		point[58].setLocation(194, 290);
    		point[59].setLocation(242, 290);
    		
    		// Board 60-67. Read area: finish.
    		point[60].setLocation(290, 242);
    		point[61].setLocation(290, 194);
    		point[62].setLocation(290, 146);
    		point[63].setLocation(290, 98);
    		point[64].setLocation(290, 50);
    		point[65].setLocation(290, 2);
    		point[66].setLocation(338, 2);
    		point[67].setLocation(386, 2);
    		
    		// Board 68-73. Red finish line.
    		point[68].setLocation(338, 50);
    		point[69].setLocation(338, 98);
    		point[70].setLocation(338, 146);
    		point[71].setLocation(338, 194);
    		point[72].setLocation(338, 242);
    		point[73].setLocation(338, 290);
    		
    		// Board 74-79. Blue finish line.
    		point[74].setLocation(626, 338);
    		point[75].setLocation(578,338);
    		point[76].setLocation(530, 338);
    		point[77].setLocation(482, 338);
    		point[78].setLocation(434, 338);
    		point[79].setLocation(386, 338);
    		
    		// Board 80-85. Yellow finish line.
    		point[80].setLocation(338, 626);
    		point[81].setLocation(338, 578);
    		point[82].setLocation(338, 530);
    		point[83].setLocation(338, 482);
    		point[84].setLocation(338, 434);
    		point[85].setLocation(338, 386);
    		
    		// Board 86-91. Green finish line.
    		point[86].setLocation(50, 338);
    		point[87].setLocation(98, 338);
    		point[88].setLocation(146, 338);
    		point[89].setLocation(194, 338);
    		point[90].setLocation(242, 338);
    		point[91].setLocation(290, 338);
    		
    	} 
    }
    
    /**
     * Throws a dice and updates the picture to show the dice thrown.
     * 
     * @param event is not used.
     */
    @FXML
    void displayThrownDiceImage(ActionEvent event) {
    	Image image;
    	
    	int dice = throwDice();
    	diceValue = dice;
    	
    	ServerConnection.sendObjectToAllClients(new ThrowDice(gameId));
    	
    	switch (dice) {
    	case 1:
            image = new Image(getClass().getResourceAsStream("/images/dice1.png"));
            diceThrown.setImage(image);
            break;
    	case 2:
            image = new Image(getClass().getResourceAsStream("/images/dice2.png"));
            diceThrown.setImage(image);
    		break;
    	case 3:
    		image = new Image(getClass().getResourceAsStream("/images/dice3.png"));
            diceThrown.setImage(image);
    		break;
    	case 4:
    		image = new Image(getClass().getResourceAsStream("/images/dice4.png"));
            diceThrown.setImage(image);
    		break;
    	case 5:
    		image = new Image(getClass().getResourceAsStream("/images/dice5.png"));
            diceThrown.setImage(image);
    		break;
    	case 6:
    		image = new Image(getClass().getResourceAsStream("/images/dice6.png"));
            diceThrown.setImage(image);
    		break;
    	default:
    		System.out.println("I am default!");
    	}
    }

    /**
     * Initializes the game and starts it with the current players.
     * 
     * @param players sends the players to StartGame.
     */
	public void initializeGame(String[] players) {
		initializeGame(new StartGame(players));
		
	}
}