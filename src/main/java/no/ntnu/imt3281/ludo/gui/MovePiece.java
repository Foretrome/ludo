package no.ntnu.imt3281.ludo.gui;

public class MovePiece {

	/**
	 * The ID of a game.
	 */
	public int gameId;
	
	/**
	 * The player moving the piece.
	 */
	public int movingPlayer;
	
	/**
	 * Where on the board the piece is moving from.
	 */
	public int moveUserPieceFrom;
	
	/**
	 * If the piece can move from start.
	 */
	public boolean canMoveFromStart;
	
	/**
	 * Instantiates the variables for this moving piece.
	 * 
	 * @param id The ID of a game.
	 * @param player The player moving the piece.
	 * @param from Where on the board the piece is moving from.
	 * @param moveFromStart If the piece can move from start.
	 */
	public MovePiece(int id, int player, int from, boolean moveFromStart) {
		this.gameId = id;
		this.movingPlayer = player;
		this.moveUserPieceFrom = from;
		this.canMoveFromStart = moveFromStart;
	}
	
	/**
	 * Returns the ID of the game.
	 * 
	 * @return gameId is the ID of a game.
	 */
	public int getGameId() {
		return gameId;
	}
	
	/**
	 * Returns the player that is moving.
	 * 
	 * @return movingPlayer is the player moving.
	 */
	public int getMovingPlayer() {
		return movingPlayer;
	}
	
	/**
	 * Returns where on the board the piece is moving from.
	 * 
	 * @return moveUserPieceFrom is what field the piece is currently standing in.
	 */
	public int getMoveUserPieceFrom() {
		return moveUserPieceFrom;
	}
	
	/**
	 * Returns if the piece can move out from start.
	 * 
	 * @return canMoveFromStart is true if the piece can move out from start and false if it can't.
	 */
	public boolean getCanMoveFromStart() {
		return canMoveFromStart;
	}
}
