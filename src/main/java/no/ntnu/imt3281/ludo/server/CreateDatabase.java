package no.ntnu.imt3281.ludo.server;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Creates a Derby database and necessary tables used for the Ludo application
 * @author Tommy
 */
public class CreateDatabase {
	
	/**
	 * The URL for the database. Does also create it with 'create=true'
	 */
    private static final String URL = "jdbc:derby:LudoDB;create=true";
    
    /**
     * Java.util Logger for handling exceptions and such
     */
    private static final Logger LOGGER = Logger.getLogger(CreateDatabase.class.getName());
    
    /**
     * Creates the associated DB table(s).
     * @param args Not used
     */
    public static void main(String[] args) {
    	createTableUsers();
    	createTableGlobalChat();
    	LOGGER.log(Level.INFO, "Finished creating DB.");
    }

    /**
     * Creates the table where all global chat messages is stored
     */
	private static void createTableGlobalChat() {
		String table = "Globalchat";
		try(
	    	Connection con = DriverManager.getConnection(URL);
	    	Statement stmt = con.createStatement()
	    	){
				DatabaseMetaData dbmd = con.getMetaData();
		    	ResultSet rs = dbmd.getTables(null, null, table.toUpperCase(),null);
		    	if (!rs.next()) {
		            stmt.execute("CREATE TABLE " + table +  "(id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
		            		+ "username varchar(12) NOT NULL, "
		            		+ "message varchar(500) NOT NULL, "
		            		+ "date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,"
		            		+ "PRIMARY KEY (id))");
		            LOGGER.log(Level.INFO, "Created table " + table);
		    	} else {
		    		LOGGER.log(Level.INFO, "Skipping creation of table " + table + ". Already exists");
		    	}
	    	} catch (SQLException sqle) {
	    		LOGGER.log(Level.SEVERE, sqle.toString(), sqle);
	    	}
	}

	/**
	 * Creats the table where all userdata is stored
	 */
	private static void createTableUsers() {
		String table = "Users";
		try(
	    	Connection con = DriverManager.getConnection(URL);
	    	Statement stmt = con.createStatement();
	    	){
				DatabaseMetaData dbmd = con.getMetaData();
		    	ResultSet rs = dbmd.getTables(null, null, table.toUpperCase(),null);
		    	if (!rs.next()) {
		            stmt.execute("CREATE TABLE " + table +  "(id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
		            		+ "username varchar(12) NOT NULL, "
		            		+ "password varchar(64) NOT NULL, "
		            		+ "firstname varchar(64) NOT NULL, "
		            		+ "lastname varchar(64) NOT NULL, "
		            		+ "emailaddress varchar(64) NOT NULL, "
		            		+ "avatarimage varchar(255) NOT NULL, "
		            		+ "PRIMARY KEY (id))");
		            LOGGER.log(Level.INFO, "Created table " + table);
		    	} else {
		    		LOGGER.log(Level.INFO, "Skipping creation of table " + table + ". Already exists");
		    	}
	    	} catch (SQLException sqle) {
	    		LOGGER.log(Level.SEVERE, sqle.toString(), sqle);
	    	}
	}
}
