package no.ntnu.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.client.Client;

/**
 * Handles the link between a Client and a socket on the server side
 * @author Tommy
 *
 */
public class ServerConnection {
	
	/**
     * Java.util Logger for handling exceptions and such
     */
    private static final Logger LOGGER = Logger.getLogger(ServerConnection.class.getName());
	Socket socket;
	BufferedReader input;
	BufferedWriter output;
	Server server;
	Client client;
	
	/**
	 * Initializes the Connection and saves the needed link
	 * @param c The client who this socket belongs to
	 * @param s The socket used to communicate with this client
	 */
	public ServerConnection(Client c, Socket s) {
		this.socket = s;
		this.client = c;
		
	}
	
	/**
	 * Sends an input message to all clients in the connections-list
	 * @param text The message to send all clients
	 */
	public static void sendStringToAllClients(String text) {
		for (int i = 0; i < Server.connections.size(); i++) {
			ServerConnection sc = Server.connections.get(i);
			sc.sendStringToClient(text);
		}
	} 

	/**
	 * Sends an input message to a single client
	 * @param text The message to be sent
	 */
	private void sendStringToClient(String text) {
		try {
			output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			output.write(text);
			output.newLine();
			output.flush();
			LOGGER.log(Level.WARNING, "ServerConnection flushed msg", text);
		} catch (IOException e) {
			
		}
	}
	
	/**
	 * Sends an input message to all clients in the connections-list.
	 * 
	 * @param o is the object to be sent.
	 */
	public static void sendObjectToAllClients(Object o) {
		for (int i = 0; i < Server.connections.size(); i++) {
			ServerConnection sc = Server.connections.get(i);
			sc.sendObjectToClient(o);
		}
	}
	
	/**
	 * Sends an input message to a single client.
	 * 
	 * @param o is the object to be sent.
	 */
	public void sendObjectToClient(Object o) {
		try {
			output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			output.write((String) o);
			output.newLine();
			output.flush();
			LOGGER.log(Level.WARNING, "ServerConnection flushed msg", o);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Object could not be sent", o);
		}
	}
}
