package no.ntnu.imt3281.ludo.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles connected with the Database.
 * @author Tommy
 *
 */
public class ConnectionManager {

	/**
	 * The URL for the database.
	 */
    private static final String URL = "jdbc:derby:LudoDB;create=false";
    
    /**
     * Java.util Logger for handling exceptions and such
     */
    private static final Logger LOGGER = Logger.getLogger(ConnectionManager.class.getName());
    private static Connection con;
    
    /**
     * Creates a connection with the database
     */
    ConnectionManager() {
    	try {
        	con = DriverManager.getConnection(URL);
        	LOGGER.log(Level.INFO, "Connected to the database server");
        } catch (SQLException ex) {
        	LOGGER.log(Level.SEVERE, "Failed to create the database connection: " + ex.toString(), ex);
        }
    }

    /**
     * Will return the DB connection
     * @return The connection to LudoDB
     */
    public static Connection getConnection() {
        return con;
    }
}
