package no.ntnu.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * This is the main class for the server.  Handles all communication with database and clients
 * @author Tommy
 *
 */
public class Server {

	/**
	 * Java.util Logger for handling exceptions and such
	 */
	private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
	private static ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
	private static ExecutorService executorService;
	private static ArrayBlockingQueue<String> messages = new ArrayBlockingQueue<>(50);
	private static ArrayBlockingQueue<String> outgoingMessages = new ArrayBlockingQueue<>(50);
	private static ArrayBlockingQueue<String> systemMessages = new ArrayBlockingQueue<>(50);
	private static ArrayBlockingQueue<String> waitingQueue = new ArrayBlockingQueue<>(4);
	static final ArrayList<Client> missingClients = new ArrayList<>();	
	static final ArrayList<Client> clients = new ArrayList<>();									// List with all users online 
	static ServerSocket serverSocket;
	static ArrayList<ServerConnection> connections = new ArrayList<>();

	/**
	 * Boots initial program, creates a link to the DB, starts all server-side threads
	 * @param args not used
	 */
	public static void main(String[] args)	{
		new ConnectionManager();
		new DatabaseQuery();

		try {
			LOGGER.log(Level.INFO, "Booting server");
			serverSocket = new ServerSocket(9876);
			executorService = Executors.newCachedThreadPool();
			startLoginAndRegisterHandler();						// Handle login and register requests in a separate thread
			checkClientForMessages();							// Check clients for new messages and add to message list
			startMessageDivider();								// Loops through all messages in the messages-list and splits then into new lists
			startSystemMessageHandler();						// Loops through all messages in the systemMessages-list and does system functionality
			startMessageSender();								// Sends all messages in the 'outgoingMessages'-list out on the different streams
			startClientCleanup();								// Loops through the list of missingClients and removes those clients from the app
			executorService.shutdown();
		} catch(IOException ioe) {
			LOGGER.log(Level.SEVERE, ioe.toString(), ioe);
			System.exit(1);
		}
	}

	/**
	 * A separate thread running on the 'missingClients'-list and removes any clients that have disconnected from
	 * any associated chat rooms and games.
	 */
	private static void startClientCleanup() {
		// TODO
		// FJERN FRA CHATTEKANALER
		// FJERN FRA SPILL
		executorService.execute(() -> {
			while (true) {
					synchronized (missingClients) {
						while (missingClients.size() > 0) {
							Client c = missingClients.remove(0);
							LOGGER.log(Level.WARNING, "Client " + c.getName() + " is missing, removing from system");
						}
					}
			}
		});
	}

	/**
	 * Loops through all messages in the messages-list and splits then into new lists according to where they should be sent
	 */
	private static void startMessageDivider() {
		executorService.execute(() -> {
			while (true) {
				try {
					String message = messages.take();
					if (message.contains(">GLOBAL>")) {
						// Re-pack message from TOKEN>GLOBAL>MESSAGE to GLOBAL>USERNAME>MESSAGE
						String[] content = message.split(">");
						String msg = content[2];													// Find the actual message
						String username = messageTokenToUsername(message);							// Find username from token in message
						String newMessage = "GLOBAL>" + username + ">" + msg;
						
						DatabaseQuery.saveGlobalMessageToDatabase(username, msg);					// Save message to DB
						outgoingMessages.put(newMessage);											// Adding to list of global messages
						System.out.println("Adding message to outgoing message queue: " + message);
					} else if (message.contains("JOINWAITINGQUEUE")) {
						waitingQueue.put(messageTokenToUsername(message));
						System.out.println("Adding message to waiting queue: " + message);
					} else if (message.contains("STATUSQUEUE")) {
						String username = messageTokenToUsername(message);
						String newMessage = "USER>" + username + ">" + waitingQueue.toString();
						outgoingMessages.put(newMessage);
						System.out.println("Checking status for queue: " + message);
						System.out.println("Currently in queue: " + waitingQueue.toString());
					} else if (message.endsWith("GETPROFILE") || message.contains(">UPDATEPROFILE")) {
						systemMessages.put(message);
						System.out.println("Adding message to system queue: " + message);
					}
				} catch (InterruptedException ie) {
					LOGGER.log(Level.WARNING, ie.toString(), ie);
					// Restore interrupted state...
					Thread.currentThread().interrupt();
				}
			}
		});
	}

	/**
	 * Loops through all messages in the systemMessages-list and does system functionality.
	 * Currently only handles Profile data.
	 */
	private static void startSystemMessageHandler() {
		executorService.execute(() -> {
			while (true) {
				try { 
					String message = systemMessages.take();
					String[] content = message.split(">");
					String token = content[0];

					if (content[1].equals("GETPROFILE")) {
						sendUserProfile(token);
					} else if (content[1].equals("UPDATEPROFILE")) {
						updateUserProfile(token, content);
					}
				} catch (InterruptedException e) {
					LOGGER.log(Level.WARNING, e.toString(), e);
					// Restore interrupted state...
					Thread.currentThread().interrupt();
				} 
			}
		});
	}

	/**
	 * Updates a users profile with new data recieved in the parameter
	 * @param token The token for the user to update
	 * @param content The content to be added in the database
	 */
	private static void updateUserProfile(String token, String[] content) {
		// Only one thread at a time might use the clients object
		synchronized (clients) {
			Iterator<Client> i = clients.iterator();
			while (i.hasNext()) {
				Client c = i.next();
				if (c.getToken().equals(token)) {
					String field = content[2];
					String value = content[3];
					switch (field) {
						case "firstname": DatabaseQuery.updateUserFirstname(c.getName(), value); break;
						case "lastname": DatabaseQuery.updateUserLastname(c.getName(), value); break;
						case "emailaddress": DatabaseQuery.updateUserEmailaddress(c.getName(), value); break;
						case "password": DatabaseQuery.updateUserPassword(c.getName(), value); break;
						default: break;
					}
				}
			}
		}
	}

	/**
	 * Will get all userdata from database and return it to the user who asked for it.
	 * @param token The user who requested the profiledata's token.
	 */
	private static void sendUserProfile(String token) {
		// Only one thread at a time might use the clients object
		synchronized (clients) {	
			Iterator<Client> i = clients.iterator();
			while (i.hasNext()) {
				Client c = i.next();
				try {
					if (c.getToken().equals(token)) {
						String fname = DatabaseQuery.getUserFirstname(c.getName());
						String lname = DatabaseQuery.getUserLastname(c.getName());
						String email = DatabaseQuery.getUserEmailaddress(c.getName());
						if (fname.equals("")|| fname== null) {
							fname = "undef";
						}
						if (lname.equals("")|| lname== null) {
							lname = "undef";
						}
						if (email.equals("")|| email== null) {
							email = "undef";
						}
						
						//PROFILE:TOKEN:USERNAME:FIRSTNAME:LASTNAME:EMAIL
						String msg = "PROFILE:"
								+ token + ":"
								+ c.getName() + ":"
								+ fname + ":"
								+ lname + ":"
								+ email;
						c.sendText(msg);
					}
				} catch (IOException ioe) {	
					// Unable to communicate with the client, add to cleanup
					i.remove();
					synchronized (missingClients) {
						missingClients.add(c);
					}
				}
			}
		}
	}

	/**
	 * Looping through all Clients in the clients list and checks if anyone has new messages in their Input.
	 * If a message is found, this message is stored in the message-list
	 */
	private static void checkClientForMessages() {
		executorService.execute(() -> {
			while (true) {
				try {
					synchronized (clients) {	// Only one thread at a time might use the clients object 
						Iterator<Client> i = clients.iterator();
						while (i.hasNext()) {
							Client c = i.next();
							try {
								BufferedReader input = new BufferedReader(new InputStreamReader(c.getConnection().getInputStream()));
								String msg = null;
								if (input.ready()) {
									msg = input.readLine();
								}
								if (msg != null) {		//if (msg != null && !msg.equals(">>>LOGOUT<<<"))
									messages.put(c.getToken()+">"+msg);
									System.out.println("Adding message to messages list: " + c.getToken()+">"+msg);
								}  
								/* else if (msg != null) {	// >>>LOGOUT<<< received, remove the client
		                            i.remove();
		                            messages.put("LOGOUT:"+c.getName());
		                            messages.put(c.getName()+" logged out");
		                        }*/
							} catch (IOException ioe) {
								// Unable to communicate with the client, add to cleanup
								i.remove();
								synchronized (missingClients) {
									missingClients.add(c);
								}
							}
						}
					}
				} catch (InterruptedException ie) {
					LOGGER.log(Level.WARNING, ie.toString(), ie);
					// Restore interrupted state...
					Thread.currentThread().interrupt();
				} 
			}
		});
	}

	/**
	 * Sends all messages in the 'outgoingMessages'-list out on the different streams
	 */
	private static void startMessageSender() {
		executorService.execute(() -> {
			while (true) {
				try {
					String message = outgoingMessages.take();
					if (message.startsWith("GLOBAL>")) {
						//ServerConnection.sendStringToAllClients(message);
						sendMessageGlobal(message);
					} else if (message.startsWith("USER>")) {
						String[] content = message.split(">");
						String username = content[1];
						String statusList = content[2];
						String userMessage = username + ">"+ statusList;
						sendMessageUser(userMessage);
					}
				} catch (InterruptedException ie) {
					LOGGER.log(Level.WARNING, ie.toString(), ie);
					// Restore interrupted state...
					Thread.currentThread().interrupt();
				}
			}
		});
	}

	/**
	 * Sends a message to all users currently logged in to the application
	 * @param message The message to broadcast
	 */
	private static void sendMessageGlobal(String message) {
		System.out.println("Sending global message \"" + message + "\" to " + clients.size() + " clients");
		// Only one thread at a time might use the clients object
		synchronized (clients) {
			Iterator<Client> i = clients.iterator();
			while (i.hasNext()) {
				Client c = i.next();
				try {
					BufferedWriter output = new BufferedWriter(new OutputStreamWriter(c.getConnection().getOutputStream()));
					output.write(message);
					output.newLine();
					output.flush();
				} catch (IOException ioe) {	
					// Unable to communicate with the client, add to cleanup
					i.remove();
					synchronized (missingClients) {
						missingClients.add(c);
					}
				}
			}
		} 
	}
	
	/**
	 * Sends a message to a single user.
	 * @param message The message to a user.
	 */
	private static void sendMessageUser(String message) {
		synchronized (clients) {		// Only one thread at a time might use the clients object
			Iterator<Client> i = clients.iterator();
			while (i.hasNext()) {
				Client c = i.next();
				try {
					c.sendText(message);
				} catch (IOException ioe) {	
					// Unable to communicate with the client, add to cleanup
					i.remove();
					synchronized (missingClients) {
						missingClients.add(c);
					}
				}
			}
		} 
	}

	/**
	 * Takes a full message directly from a stream and locates the token in this message.
	 * Once token is found, it will look for a match in the clients-list and find this clients username and return it.
	 * @param message The full message straight from stream
	 * @return The username of the user who sent this message
	 */
	private static String messageTokenToUsername(String message) {
		// Finds token in message
		String[] content = message.split(">");
		String token = content[0];
		String username = null;
		
		// Find username from token
		synchronized (clients) {		// Only one thread at a time might use the clients object
			Iterator<Client> i = clients.iterator();
			while (i.hasNext()) {
				Client c = i.next();
				if(c.getToken().equals(token)) {
					username = c.getName();
				}
			}
		}
		return username;
		
	}

	/**
	 * Function is constantly running on a separate thread, listening for any attempts to connect to this server.
	 * If the connection is made, an input will be received for either login or register, and either way this functionality is
	 * also handled in this function
	 */
	private static void startLoginAndRegisterHandler() {
		executorService.execute(() -> {
			LOGGER.log(Level.INFO, "Server booted, Waiting for packets..");
			while (true) {
				try {
					Socket socketToClient = serverSocket.accept();
					LOGGER.log(Level.INFO, "Connection accepted.");
					BufferedReader input = new BufferedReader(new InputStreamReader(socketToClient.getInputStream()));
					BufferedWriter output = new BufferedWriter(new OutputStreamWriter(socketToClient.getOutputStream()));

					String line = input.readLine();
					String[] content = line.split(":");
					String operation = content[0];
					String username = content[1];
					String password = content[2];
					
					switch (operation) {
					case "LOGIN":
						loginUser(input, output, socketToClient, username, password);
						break;
					case "REGISTER":
						registerNewUser(output, username, password);
						break;
					}   
				} catch (IOException ioe) {
					LOGGER.log(Level.SEVERE, ioe.toString(), ioe);
				}
			}
		});
	}

	/**
	 * Will attempt to log in a user to the Ludo server and create a new Client object with the users data.
	 * @param input The input stream for this Client/user
	 * @param output The output stream for this Client/user
	 * @param socketToClient The connection for this Client/user
	 * @param username The username for this Client/user
	 * @param password The password for this Client/user
	 */
	private static void loginUser(BufferedReader input, BufferedWriter output, Socket socketToClient, String username, String password) {	
		String response = "NOT:NOT";								// Dummy response message
		try(PreparedStatement pstmt = DatabaseQuery.preparedStatementSelectUser(username);
				ResultSet resultSet = pstmt.executeQuery();
				){
			if (resultSet.next()) {										// Finds username in database results
				String pwd = resultSet.getString(3);
				if (password.equals(pwd)) {								// If password matches
					String token = UUID.randomUUID().toString();		// Create unique token
					synchronized (clients) {

						// Create new Client and add to client-list
						Client c = new Client();
						c.setName(username);
						c.setToken(token);
						c.setConnection(socketToClient);
						c.setInput(input);
						c.setOutput(output);
						clients.add(c);
						
						// Save link between client and socket
						ServerConnection sc = new ServerConnection(c, socketToClient);
						connections.add(sc);
						
						response = "LOGGEDIN:" + token;
						LOGGER.log(Level.INFO, "Created new token and added user to Client list", c);
					}
				} else {
					LOGGER.log(Level.WARNING, bundle.getString("system.wrongPassword"));
				}
			} else {
				LOGGER.log(Level.WARNING, bundle.getString("system.usernameNotFound"));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		
		// Send response and token to client
		try {
			output.write(response);
			output.newLine();
			output.flush();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}

	/**
	 * Adds a new user to the Ludo database
	 * @param output The channel on where to send response (added/not)
	 * @param username The username for the new user
	 * @param password The password for the new user
	 */
	private static void registerNewUser(BufferedWriter output, String username, String password) {
		String response = "NOT";									// Dummy response message
		try(PreparedStatement pstmt = DatabaseQuery.preparedStatementSelectUser(username);
				ResultSet resultSet = pstmt.executeQuery();
				){
			if(!resultSet.next()){										// If unique username
				try(PreparedStatement pre = DatabaseQuery.preparedStatementCreateUser(username, password);
						){
					pre.executeUpdate();
					response = "REGISTERED";							// Response message
					LOGGER.log(Level.INFO, "Added user to database.", username);
				} catch (SQLException e) {
					LOGGER.log(Level.SEVERE, e.toString(), e);
				}
			} else {
				LOGGER.log(Level.WARNING, bundle.getString("system.usernameDuplicate"));
			}
		} catch (SQLException e1) {
			LOGGER.log(Level.SEVERE, e1.toString(), e1);
		}

		// Send confirmation to client
		try {
			output.write(response);
			output.newLine();
			output.flush();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}
}
