package no.ntnu.imt3281.ludo.server;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles most database logic and requests sent from server
 * @author Tommy
 *
 */
public class DatabaseQuery {
	
	/**
	 * Java.util Logger for handling exceptions and such
	 */
	private static final Logger LOGGER = Logger.getLogger(DatabaseQuery.class.getName());
	private static Connection con = null;
	
	/**
	 * Initializes a DatabaseQuery object and sets the DB connection.
	 */
	public DatabaseQuery() {
		con = ConnectionManager.getConnection();
	}

	/**
	 * Function will save a global message in the Globalchat table
	 * @param username The username for the user who wrote the message
	 * @param message The full message string from the stream
	 */
	static void saveGlobalMessageToDatabase(String username, String message) {
		try(PreparedStatement pre = preparedStatementAddGlobalChatMessage(username, message);
				){
			int i = pre.executeUpdate();
			if (i > 0 ) {
				LOGGER.log(Level.INFO, "Added global chat message to Database", message);
			}
			pre.close();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}
	
	/**
	 * Fuction to retrieve a given users emailaddress from the database
	 * @param username The username of the user to get data from
	 * @return The emailaddress
	 */
	static String getUserEmailaddress(String username) {
		String email = null;
		try(PreparedStatement pstmt = preparedStatementSelectUserEmailaddress(username);
				ResultSet resultSet = pstmt.executeQuery();
				){
			if (resultSet.next()) {										// Finds username in database results
				email = resultSet.getString(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return email;
	}

	/**
	 * Fuction to retrieve a given users lastname from the database
	 * @param username The username of the user to get data from
	 * @return The lastname
	 */
	static String getUserLastname(String username) {
		String lastname = null;
		try(PreparedStatement pstmt = preparedStatementSelectUserLastname(username);
				ResultSet resultSet = pstmt.executeQuery();
				){
			if (resultSet.next()) {
				lastname = resultSet.getString(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return lastname;
	}

	/**
	 * Fuction to retrieve a given users firstname from the database
	 * @param username The username of the user to get data from
	 * @return The firstname
	 */
	static String getUserFirstname(String username) {
		String firstname = null;
		try(PreparedStatement pstmt = preparedStatementSelectUserFirstname(username);
				ResultSet resultSet = pstmt.executeQuery();
				){
			if (resultSet.next()) {
				firstname = resultSet.getString(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return firstname;
	}

	/**
	 * Function will update the 'emailaddress' field on a given user
	 * @param username The username of the user to update
	 * @param emailaddress The new emailaddress to set on the user
	 */
	static void updateUserEmailaddress(String username, String emailaddress) {
		try(PreparedStatement pre = preparedStatementUpdateUserEmailaddress(username, emailaddress);
				){
			int i = pre.executeUpdate();
			if (i > 0 ) {
				LOGGER.log(Level.INFO, "Updated column 'emailaddress' for user", username);
			}
			pre.close();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}

	/**
	 * Function will update the 'lastname' field on a given user
	 * @param username The username of the user to update
	 * @param lastname The new lastname to set on the user
	 */
	static void updateUserLastname(String username, String lastname) {
		try(PreparedStatement pre = preparedStatementUpdateUserLastname(username, lastname);
				){
			int i = pre.executeUpdate();
			if(i > 0) {
				LOGGER.log(Level.INFO, "Updated column 'lastname' for user", username);
			}
			pre.close();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}

	/**
	 * Function will update the 'firstname' field on a given user
	 * @param username The username of the user to update
	 * @param firstname The new firstname to set on the user
	 */
	static void updateUserFirstname(String username, String firstname) {
		try(PreparedStatement pre = preparedStatementUpdateUserFirstname(username, firstname);
				){
			int i = pre.executeUpdate();
			if(i > 0) {
				LOGGER.log(Level.INFO, "Updated column 'firstname' for user", username);
			}
			pre.close();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}

	/**
	 * Function will hash and update the 'password' field on a given user
	 * @param username The username of the user to update
	 * @param password The new password to set on the user
	 */
	static void updateUserPassword(String username, String password) {
		String hash = hashPassword(password);
		try(PreparedStatement pre = preparedStatementUpdateUserPassword(username, hash);
				){
			int i = pre.executeUpdate();
			if(i > 0) {
				LOGGER.log(Level.INFO, "Updated column 'password' for user", username);
			}
			pre.close();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}
	
	/**
	 * Hashes the password with SHA-1
	 * @param password The password to be hashed
	 * @return The finished hash
	 */
	public static String hashPassword(String password) {
		String hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.update(password.getBytes(), 0, password.length());
			hash = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return hash;
	}

	/**
	 * Will build and return a prepared statement to retrieve all data on a given user from the Users table
	 * @param username The user to get data on
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementSelectUser(String username) throws SQLException {
		String sql = "SELECT * from Users where username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to create a new user in the Users table
	 * @param username The username for the new user
	 * @param password The password for the new user
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementCreateUser(String username, String password) throws SQLException {
		String sql = "INSERT INTO Users "
				+ "(username, password, firstname, lastname, emailaddress, avatarimage) "
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, username);
		ps.setString(2, password);
		ps.setString(3, "");					// Empty lines for now. Will add functionality to edit profile later.
		ps.setString(4, "");
		ps.setString(5, "");
		ps.setString(6, "");
		return ps;
	}

	/**
	 * Will build and return a prepared statement to update a users firstname in the Users table
	 * @param username The username for the new user
	 * @param firstname The updated firstname for the new user
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementUpdateUserFirstname(String username, String firstname) throws SQLException {
		String sql = "UPDATE Users "
				+ "SET firstname = ?"
				+ "WHERE username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, firstname);
		ps.setString(2, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to update a users lastname in the Users table
	 * @param username The username for the new user
	 * @param lastname The updated lastname for the new user
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementUpdateUserLastname(String username, String lastname) throws SQLException {
		String sql = "UPDATE Users "
				+ "SET lastname = ?"
				+ "WHERE username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, lastname);
		ps.setString(2, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to update a users emailaddress in the Users table
	 * @param username The username for the new user
	 * @param email The updated emailaddress for the new user
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementUpdateUserEmailaddress(String username, String email) throws SQLException {
		String sql = "UPDATE Users "
				+ "SET emailaddress = ?"
				+ "WHERE username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, email);
		ps.setString(2, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to update a users password in the Users table
	 * @param username The username for the new user
	 * @param hash The updated and hashed password for the new user
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementUpdateUserPassword(String username, String hash) throws SQLException {
		String sql = "UPDATE Users "
				+ "SET password = ?"
				+ "WHERE username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, hash);
		ps.setString(2, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to select a users firstname from the Users table
	 * @param username The username for the user to select data from
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementSelectUserFirstname(String username) throws SQLException {
		String sql = "SELECT firstname from Users where username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to select a users emailaddress from the Users table
	 * @param username The username for the user to select data from
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementSelectUserEmailaddress(String username) throws SQLException {
		String sql = "SELECT emailaddress from Users where username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, username);
		return ps;
	}

	/**
	 * Will build and return a prepared statement to select a users lastname from the Users table
	 * @param username The username for the user to select data from
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementSelectUserLastname(String username) throws SQLException {
		String sql = "SELECT lastname from Users where username = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, username);
		return ps;
	}
	
	/**
	 * Will build and return a prepared statement to add a global chat message to the GlobalChat table
	 * @param username The username for the user that wrote the message
	 * @param message The actual message the user wrote
	 * @return Returns a preparedstatement ready to be executed
	 * @throws SQLException Any potential SQL errors
	 */
	static PreparedStatement preparedStatementAddGlobalChatMessage(String username, String message) throws SQLException {
		String sql = "INSERT INTO Globalchat "
				+ "(username, message) "
				+ "VALUES (?, ?)";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, username);
		ps.setString(2, message);
		return ps;
	}
}
